import unittest


class TestFlaskApp(unittest.TestCase):

    def test_flask_instance(self):
        from flask import Flask
        from dataserver.flaskapp import app
        assert isinstance(app, Flask)

    def test_contains_data(self):
        from dataserver.flaskapp import app
        from dataserver.tempdata import RegionTemp
        assert isinstance(app.tempdata, RegionTemp)


if __name__ == '__main__':
    unittest.main()