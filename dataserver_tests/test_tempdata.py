import unittest
from io import StringIO

from pandas import DatetimeIndex


class TestRegionTemp(unittest.TestCase):
    csv_head = "Date;Code INSEE région;Région;TMin (°C);TMax (°C);TMoy (°C)\n" \
               "2016-03-09;94;Corse;3.75;12.8;8.28"

    def test_load_csv(self, ):

        from dataserver.tempdata import RegionTemp
        rt = RegionTemp(StringIO(self.csv_head))

        assert len(rt.dataframe) == 1

    def test_tmax_access(self):
        from dataserver.tempdata import RegionTemp
        rt = RegionTemp(StringIO(self.csv_head))

        assert isinstance(rt.tmax.index, DatetimeIndex)
        assert (rt.tmax["tmax"] == rt.dataframe.tmax).all()

    def test_tmin_access(self):
        from dataserver.tempdata import RegionTemp
        rt = RegionTemp(StringIO(self.csv_head))

        assert isinstance(rt.tmin.index, DatetimeIndex)
        assert (rt.tmin["tmin"] == rt.dataframe.tmin).all()

    def test_tavg_access(self):
        from dataserver.tempdata import RegionTemp
        rt = RegionTemp(StringIO(self.csv_head))

        assert isinstance(rt.tavg.index, DatetimeIndex)
        assert (rt.tavg["tavg"] == rt.dataframe.tavg).all()


if __name__ == '__main__':
    unittest.main()
