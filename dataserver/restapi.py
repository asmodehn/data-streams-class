from dataserver.flaskapp import app
import pandas as pd


@app.route("/api")
def hello_api():
    return "Hello API!"


##########  Step 1

# Whole csv data (reprocessed to guarantee consistent format)
@app.route("/api/csv")
def csv():
    return app.tempdata.dataframe.to_csv()

# Basic route to an indexed column
@app.route("/api/tmin")
def tmin():
    return app.tempdata.tmin


@app.route("/api/tmax")
def tmax():
    return app.tempdata.tmax


@app.route("/api/tavg")
def tavg():
    return app.tempdata.tavg


# dynamic region based route
@app.route("/api/<region>")
def region(region):
    # filter
    filtered = app.tempdata.dataframe[app.tempdata.dataframe.region == region]
    # convert timestamp to string to store in python dict
    filtered.index = pd.to_datetime(filtered.index).astype(str)
    return filtered.to_dict(orient="index")


if __name__ == '__main__':
    from flask import Flask

    # Make sure the app is created as expected
    assert isinstance(app, Flask)

    # Do an example request
    cl = app.test_client()
    print(cl.get('/api/Corse').json)


