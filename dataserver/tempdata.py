# -*- coding: utf-8 -*-

import pandas as pd


class RegionTemp:

    dataframe: pd.DataFrame

    def __init__(self, csv_file):

        self.dataframe = pd.read_csv(csv_file, sep=";")

        self.dataframe.rename(columns={
            'Date': 'date',
            'Code INSEE région': 'code_insee',
            'Région': 'region',
            'TMin (°C)': 'tmin',
            'TMax (°C)': 'tmax',
            'TMoy (°C)': 'tavg',
        }, inplace=True)

        self.dataframe["date"] = pd.to_datetime(self.dataframe["date"])

        self.dataframe.set_index(["date"], drop=True, inplace=True)
        pass

    @property
    def code_insee(self):
        return self.dataframe["code_insee"].to_frame()

    @property
    def region(self):
        return self.dataframe["region"].to_frame()

    @property
    def tmax(self):
        return self.dataframe["tmax"].to_frame()

    @property
    def tmin(self):
        return self.dataframe["tmin"].to_frame()

    @property
    def tavg(self):
        return self.dataframe["tavg"].to_frame()

    # table vizualisation
    @property
    def html(self):
        return self.dataframe.to_html()


if __name__ == '__main__':
    import os

    dir_path = os.path.dirname(os.path.realpath(__file__))

    rt = RegionTemp(os.path.join(dir_path, "temperature-quotidienne-regionale.csv"))

    print(rt.dataframe.head())
