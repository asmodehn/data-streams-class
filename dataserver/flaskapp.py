import asyncio
import os


from flask import Flask, render_template

from bokeh.embed import server_document

app = Flask(__name__)

from dataserver.tempdata import RegionTemp

dir_path = os.path.dirname(os.path.realpath(__file__))

app.tempdata = RegionTemp(os.path.join(dir_path, "temperature-quotidienne-regionale.csv"))


####### Step 1


@app.route("/")
def index():
    return render_template("index.html", message="Hello Flask!")


######## Step 4


@app.route("/table")
def table():
    return render_template("table.html", tables=[app.tempdata.html], message="Hello Flask!")


# Note : the website could use the REST API as well

def build_bokeh_page(bkapp_url):
    """ Dynamically add page to the flask app """
    @app.route('/plot', methods=['GET'])
    def bkapp_page():
        script = server_document(bkapp_url)
        return render_template("plot.html", script=script, template="Flask")


if __name__ == '__main__':
    # Make sure the app is created as expected
    assert isinstance(app, Flask)

    # Do an example request with the test client only
    cl = app.test_client()
    print(cl.get('/').data)
