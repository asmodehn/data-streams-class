import os
import asyncio
import threading

from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from bokeh.application import Application
from bokeh.application.handlers import FunctionHandler
from bokeh.layouts import column
from bokeh.models import ColumnDataSource, Slider
from bokeh.plotting import figure
from bokeh.sampledata.sea_surface_temperature import sea_surface_temperature
from bokeh.server.server import BaseServer
from bokeh.server.tornado import BokehTornado
from bokeh.server.util import bind_sockets
from bokeh.themes import Theme

import os
from bokeh.layouts import column, row
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource, Slider, Div, RangeTool
from bokeh.models.widgets import CheckboxGroup, RadioButtonGroup, RadioGroup
from bokeh.io import output_notebook
from IPython.core.debugger import set_trace  # to debug


def bkapp_sample(doc):
    """ The core of the bokeh application, building a doc from some data."""
    df = sea_surface_temperature.copy()
    source = ColumnDataSource(data=df)

    plot = figure(x_axis_type='datetime', y_range=(0, 25), y_axis_label='Temperature (Celsius)',
                  title="Sea Surface Temperature at 43.18, -70.43")
    plot.line('time', 'temperature', source=source)

    # Because the callback is PYTHON, it has to run on the SERVER.
    def callback(attr, old, new):
        if new == 0:
            data = df
        else:
            data = df.rolling('{0}D'.format(new)).mean()
        source.data = ColumnDataSource.from_df(data)
    # We could have custom JS callbacks that run on the client only.
    # One solution or the other will be chosen depending on your requirements.

    slider = Slider(start=0, end=30, value=0, step=1, title="Smoothing by N Days")
    slider.on_change('value', callback)

    doc.add_root(column(slider, plot))

    thm_path = os.path.join(os.path.dirname(__file__), 'theme.yaml')
    doc.theme = Theme(filename=thm_path)


def cleandata():
    # # Retrieving Data

    from dataserver.tempdata import RegionTemp
    dir_path = os.path.dirname(os.path.realpath(__file__))
    tempdata = RegionTemp("temperature-quotidienne-regionale.csv")

    # # Splitting per Code INSEE

    # +
    df = tempdata.dataframe
    tpt_per_code = dict()

    # better by code insee to sidestep potential spelling mistakes
    for c in df.code_insee.unique():
        tpt_per_code[c] = df[df['code_insee'] == c].sort_index()
        # Dont forget sorting !

    # Data example (Occitanie)
    tpt_per_code[76].head()
    # -

    # # Filtering relevant data, per region

    # +

    tpt_per_region = dict()
    for k, d in tpt_per_code.items():
        # print(k)
        # building temperature per region dict
        r = d.region.unique()
        if len(r) != 1:
            raise RuntimeError(f"Too many regions for {k} : {r}")
        else:
            # Note : we could also build code <-> region maps if needed
            tpt_per_region[r[0]] = d[["tmin", "tavg", "tmax"]].sort_index()
            # Dont forget sorting !

    # Data example
    tpt_per_region["Occitanie"].head()
    # -

    return tpt_per_region


def bkapp_tempdata_occitanie(doc):

    tpt_per_region = cleandata()
    # output_notebook()

    # # Plotting

    # plot scales original aspect based on available width
    plot = figure(plot_height=240, tools="xpan", toolbar_location=None,
                  x_range=(tpt_per_region["Occitanie"].index[0], tpt_per_region["Occitanie"].index[100]),
                  x_axis_type="datetime", x_axis_location="above",
                  background_fill_color="#efefef", sizing_mode="scale_width")
    plot.vline_stack(["tmin", "tavg", "tmax"], x="date", source=tpt_per_region["Occitanie"],
                     color=("blue", "green", "red"))
    # show(plot)

    # # Adding RangeTool for interactive visualization

    select = figure(title="Drag the middle and edges of the selection box to change the range above",
                    plot_height=130, sizing_mode="scale_width", y_range=plot.y_range,
                    x_axis_type="datetime", y_axis_type=None,
                    tools="", toolbar_location=None, background_fill_color="#efefef")

    # sharing range is enough to link behavior
    range_tool = RangeTool(x_range=plot.x_range)
    range_tool.overlay.fill_color = "navy"
    range_tool.overlay.fill_alpha = 0.2

    select.line(x='date', y='tavg', source=tpt_per_region["Occitanie"])
    # select.line(x='date', y='tavg', source=tpt_per_region["Occitanie"].resample("W").mean())
    select.ygrid.grid_line_color = None
    select.add_tools(range_tool)
    select.toolbar.active_multi = range_tool

    doc.add_root(column(plot, select, sizing_mode="scale_width"))


def bkapp_tempdata(doc):
    # +
    # Ref : https://docs.bokeh.org/en/latest/docs/user_guide/interaction.html

    tpt_per_region = cleandata()

    from bokeh.palettes import turbo

    multiplot = figure(plot_height=240, tools="xpan", toolbar_location=None,
                       # here we take Occitanie as example to get a x_range, but any would do
                       # since we have the similar X dataseries for all)
                       x_range=(tpt_per_region["Occitanie"].index[0], tpt_per_region["Occitanie"].index[100]),
                       x_axis_type="datetime", x_axis_location="above",
                       background_fill_color="#efefef", sizing_mode="scale_width")

    for (region, data), color in zip(tpt_per_region.items(), turbo(len(tpt_per_region))):
        multiplot.line(y="tavg", x="date", source=data, color=color, legend_label=region)

    multiplot.legend.location = "top_left"
    multiplot.legend.click_policy = "hide"

    # show(column(multiplot, sizing_mode="scale_width"))

    # +

    multiselect = figure(title="Drag the middle and edges of the selection box to change the range above",
                         plot_height=130, sizing_mode="scale_width", y_range=multiplot.y_range,
                         x_axis_type="datetime", y_axis_type=None,
                         tools="", toolbar_location=None, background_fill_color="#efefef")

    # sharing range is enough to link behavior
    multirange_tool = RangeTool(x_range=multiplot.x_range)
    multirange_tool.overlay.fill_color = "green"
    multirange_tool.overlay.fill_alpha = 0.2

    multiselect.line(x='date', y='tavg', source=tpt_per_region["Occitanie"])
    multiselect.ygrid.grid_line_color = None
    multiselect.add_tools(multirange_tool)
    multiselect.toolbar.active_multi = multirange_tool

    doc.add_root(column(multiplot, multiselect, sizing_mode="scale_width"))


# can't use shortcuts here, since we are passing to low level BokehTornado
bkapp = Application(FunctionHandler(bkapp_tempdata))


# Note : use bind_sockets to create the sockets
def bk_server(sockets, host, port, applications, extra_websocket_origins=None):

    if not threading.current_thread() is threading.main_thread():
        # we need to manually create an event loop when we are not in the main thread
        asyncio.set_event_loop(asyncio.new_event_loop())

    if extra_websocket_origins is None:
        extra_websocket_origins = []

    # appending direct connection for development/debugging puposes
    extra_websocket_origins.append(f"{host}:{port}")

    # building tornado server for bokeh
    bokeh_tornado = BokehTornado(applications, extra_websocket_origins=extra_websocket_origins)
    bokeh_http = HTTPServer(bokeh_tornado)
    bokeh_http.add_sockets(sockets)

    # creating eventloop and instantiating bokeh server
    server = BaseServer(IOLoop.current(), bokeh_tornado, bokeh_http)

    print(f"Bokeh listening on http://{host}:{port}...")
    print(f"WS Connection expected from {[e for e in extra_websocket_origins]}")
    server.start()
    server.io_loop.start()


if __name__ == '__main__':

    sockets, port = bind_sockets("localhost", 0)
    try:
        bk_server(sockets, host="localhost", port=port, applications={'/bkapp': bkapp})
    except KeyboardInterrupt as ke:
        print(f"KeyboardInterrupt: Exiting.")
