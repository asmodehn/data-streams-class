
from threading import Thread
from dataserver.flaskapp import build_bokeh_page
from dataserver.restapi import app  # the modified app after adding restapi routes
from dataserver.bokehapp import bkapp, bk_server, bind_sockets


# Flask server setup
app_port = 8000

# Bokeh Server run
sockets, bkport = bind_sockets("127.0.0.1", 0)


def bk_serverthread():
    import asyncio
    # need a new eventloop in this thread to be able to run tornado
    asyncio.set_event_loop(asyncio.new_event_loop())

    bk_server(sockets, host="127.0.0.1", port=bkport, applications={"/bkapp": bkapp}, extra_websocket_origins=[f"127.0.0.1:{app_port}"])


bkt = Thread(target=bk_serverthread, daemon=True)  # daemon True to terminate the thread when python interpreter finishes.
bkt.start()

# building the flask page based on the bokeh server address
build_bokeh_page(bkapp_url=f'http://127.0.0.1:{bkport}/bkapp')

# Running the flask app in main thread
#app.run(debug=True, port=app_port)  # careful port of flask and tornado must not collide !

app.run(host="0.0.0.0", debug=False, port=app_port)  # careful port of flask and tornado must not collide !
