# data-streams-class

Data Streams class


## HOWTO

Scrapping:
 - use jupyter notebooks with python scripts: use https://github.com/mwouts/jupytext 

Hosting:
 - Use flask to learn how to use a simple webserver in python. Full Tutorial : https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
 - Integrate a bokeh server for visualization (using panel) Ref: https://github.com/bokeh/bokeh/tree/master/examples/howto/server_embed


## ROADMAP
 - provide network access to the notebook: use https://github.com/voila-dashboards/voila to run the code and generate visualization

 - Access a local voila notebook server from the world wide web : install and setup http://ngrok.com

