Network Primer
==============

On a (POSIX) machine, we uniquely identify some data (a file) via a filepath (kind of an *address*)

On the network, there are many things that can interfere with the data, on our way to access it.
Some are visible and explicited in a URL (Uniform Resource Locator), some are not (switches, firewall, extra packet data, etc.)

URL
---

https://en.wikipedia.org/wiki/URL

URI generic syntax ::

    URI = scheme:[//authority]path[?query][#fragment]

    authority = [userinfo@]host[:port]


Locality: DNS - DHCP
--------------------

The **host** part of the URL is usually what will give us the physical location of some data.

It is translated from a name to an IP address via the Domain Name System : https://en.wikipedia.org/wiki/Domain_Name_System

Then that IP is physically routed by the network, dynamically discovering the best network path, step by step.

GeoIP services can link an IP and a geolocation. Ex: https://geoip.com/


Service: Protocol HTTP - FTP - etc.
-----------------------------------

On top of this "physical" exchange of information over the network, various protocols (ways to represent data and agreed process to exchange information) can be used.

The most widely used one is arguably HTTP, but there are many others. The standardised ones have one or more specific dedicated port number for them : https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xml


Consequences
------------

- The network can be more or less simulated locally (by implementing TCP connections from your machine to your machine : 127.0.0.1).
- Local connection will **never** be a complete representation of the reality, as it is somewhat and "ideal" usecase. The planetary network is much more messy ang ugly.
- A developer can start developing an application "in the abstract" locally, but keeping it running optimally on a server on the network is a never ending endeavour.

As a direct consequence, a developer, while working locally, must make extra effort to always be clear on **what is running on the browser/client /vs/ what is running on the server** for various reason (speed, reactivity, usability, law constraints, etc.)
