Programming with Python
=======================

The official python Tutorial is here : https://docs.python.org/3/tutorial/index.html

The wikipedia page, which provide a slightly different perspective is there: https://en.wikipedia.org/wiki/Python_syntax_and_semantics

We will here do a quick walkthrough among the most essential features.


Core Principles
---------------

Python was designed to be a highly readable language.
Python aims to be simple and consistent in the design of its syntax, encapsulated in the mantra "There should be one—and preferably only one—obvious way to do it".

There are other mantras, aiming to provide guidance to the python developer, listed in the zen of python: https://en.wikipedia.org/wiki/Zen_of_Python


First Keywords
--------------
https://docs.python.org/3/reference/lexical_analysis.html#keywords

Python is an imperative language, with statements and expressions, and some syntax rules.

It has keywords that cannot be used for anything other than their original meaning.
The most important one is arguably **lambda**. It allows you to define a procedure (also incorrectly called a function), and computer science has demonstrated that you can do *everything* with this one keyword.

However it would not be very readable, so python also has **def** and **return** that allow to define procedures over many lines.

There are many other, and we will see some later in this walkthrough.

Indentation
-----------
https://en.wikipedia.org/wiki/Python_syntax_and_semantics#Indentation

Python syntax is quite complex, but given its focus on readability, quite easy to learn by practice, comparatively to other programming languages.

Most notably in python the level of indentation is important, and describe the level of nesting of your control-flow blocks.


Literals
--------

Python support many kinds of literals, where you write directly the data you want the program to manipulate, instead of writing a procedure that represent it...
Most notably strings can be written with quotes::

    "A string in python"

And number can be written in various ways, for example::

    3.14
    1_000_000


Variables
---------

When we want to refer to many literal values, Python has variables.
These are just a memory pointer to where, at run time, the value will be stored::

    x = 2
    
    x + 3
    5
    
    x + 4
    6

    x = 6
    x + 2
    8
     
Although not really useful in a simple "calculator", these become necessary when definining procedures.

Note: A Python "variable" is just a stored value, and **not exactly a mathematical variable**.
But for the purpose of writing imperative code, they play a similar role.

Functions
---------
https://docs.python.org/3/library/functions.html

The most essential built-in python functions, outside of datatype constructors, are:
  - **help()** shows the help/the associated documentation of some data
  - **id()**  shows the unique identifier in memory of some data
  - **type()**  shows the type of some data
  - **print()** shows the data itself

A developer can define his own procedures, just by composing some keywords, some literals and basic functions::

    def show(a):
        print(f"{a} has type{type(a)}")


Note : A Python "function" is just a procedure, and **not a mathematical function**.
A mathematical function can be implemented as a procedure, but a procedure can potentially do much more, and it may not always be a good thing.

Datatypes
---------
https://docs.python.org/3/library/datatypes.html

The most essential built-in python datatypes are:
  - **int**: to represent integers (up to infinity in theory).
  - **set**: to represent a set of data.
  - **tuple**: to represent a pair, a triple, etc.
  - **list**: to represent a list of data.
  - **str**: to represent a Unicode string, or a list of "characters".
  - **dict**: to represent a "mapping", or an indexed collection of data.

Python has built-in functions for creating these when you don't want to write a literal.


note that passing a variable number of arguments to a procedure is done via **tuple** and **dict**, with a special synax::

        def myproc(*args, **kwargs):
            print(args)
            print(kwargs)


Booleans
--------

Boolean values **bool** is a subclass of **int** and are so useful that we have many keywords:
 - **bool()**
 - **False**
 - **True**
 - **not**
 - **is**
 - **if**
 - **elif**
 - **and**
 - **or**

Only with Booleans, we can already do impressive things.
The whole science field of Boolean Logic can be implemented only with booleans and functions over booleans.
They can be used to represent binary code, so everything doable with one is doable with the other.

We also have some built-in functions:
 - **all()**
 - **any()**
 - **filter()**

and we can build many more!


Integers
--------

Integers values are extremely useful, and indeed the python interpretor can be used as a calculator.
 - **int()**

With integers we can do even more. Some built-in functions are also dedicated to them:
 - **abs()**
 - **round()**
 - **min()**
 - **max()**
 - **divmod()**
 - **sum()**
 - etc.



Exercises
---------

To become familiar with a language, on eneed to practice it regularly.
You can find online a large variety of Coding Katas, like the ones from http://codingdojo.org/kata/


Factorial
^^^^^^^^^

Write a function that compute factorial of an integer:

 - factorial(2) = 2
 - factorial(3) = 6
 - factorial(4) = 24
 - etc.

FizzBuzz
^^^^^^^^

Write a program that prints the numbers from 1 to 100.
But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
For numbers which are multiples of both three and five print “FizzBuzz“.


Roman numerals
^^^^^^^^^^^^^^

Write a program that prints the numbers from 1 to 420, in Roman Numerals, that would be from I to CDXX
Reference : http://www.novaroma.org/via_romana/numbers.html


Exceptions
----------
To go further, python also as the concept of **Exception**, as well as some keywords related to it:
  - **raise** to raise an exception. This stop the code block in place and raise the exception to the caller(s) until someone catches it or the program stops.
  - **try** to start a block, expecting some exception to be raised
  - **except** to declare which exceptions we want to catch
  - **finally** to declare a common code that will be executed on exit of the block, no matter which exception was raised.

https://wiki.python.org/moin/HandlingExceptions

That concept allows the program to be aware of an error, and react to it, dynamically, without crashing completely.

The most used Exception is **KeyboardInterrupt**.
The default CPython interpreter catches any Ctrl-C keypress from the user, and returns a KeyboardInterrupt Exception when it occurs.
So the developer can simply react to unexpected program interruption by the user with Ctrl-C by reacting to that Exception.


Decorators
----------

A decorator is just a higher-order function. It is applied to a function and return a function::

    def mydecorator(myfun):
    
        def wrapper(*args, **kwargs):
            print(f"Calling {myfun} with *{args} and **{kwargs}..."
            return myfun(*args, **kwargs)
            
        return wrapper
      
Python has a specific syntax for these, that makes it easy to modify the code at import time::

    @mydecorator
    def addone(x):
        return x+1
        
        
This design is very powerful and used by many libraries. It is also quite intuitive to use.


Iterators
---------

To go further than functions as a basic programming construct, python also has iterators, and they are supported at the very basic level in python.
  - **iter()** will create an iterator from an iterable object (like a **range** object)
  - **next()** will grab the next value in the iterator
  - built-in data structures can behave like iterators (set, list, etc.)

An iterator is an object that implements **next**, which is expected to return the next element of the iterable object that returned it,
and raise a StopIteration exception when no more elements are available.
https://wiki.python.org/moin/Iterator

Generators
----------

https://wiki.python.org/moin/Generators

Generator are procedure that you can call **once** but will **yield** a value **multiple times** before returning.
They effectively allow to write procedures that "behave like iterators"::

    def my1to5gen():
        yield 1
        yield 2
        yield 3
        yield 4
        yield 5


Loops
-----

Another widely known general construct for a program is the loop. It is not mandatory in a program, however it provides good readability and usability for the developer.
  - **for** is used to loop in a sequence
  - **in** is used to check if a value exist in an iterable, or to iteratively draw from the iterator values for a loop.
  - **while** will loop if the condition is still true.


Exercises
---------

You can attempt to rewrite the previous Katas with generators and loops.


Context Managers
----------------

Yet Another useful programming construct in python is the Context Manager.
It is basically an object, that we pass via a **with** keyword, that guarantee the execution of an action whenever the control flow exits that block, as usual, vian an exception, etc.
This is very useful when dealing with various resources(network, files, etc.) to make sure the necessary "cleanup actions" are always done.

It is also very easy to define a Context Manager from a generator::

    import contextlib
    @contextlib.contextmanager
    def withcomments(myfun):
        print("Entering context...")
        yield  
        # whatever you yield here will be passed as a variable 
        # if you use the with ... as ... syntax
        print ("Leaving Context...")
        

