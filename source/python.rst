Python
======

Python is a generalist, versatile, widely used programming language.
Here we review the basics necessary for this class.

Python2 has been around for 20 years and is now obsolete, as maintenance on it has ended.
Therefore we will only talk about python3
https://en.wikipedia.org/wiki/History_of_Python

Basics
------

Python is an **imperative** **interpreted** **reflective** programming language.

- **imperative**: you describe actions that you want the machine to take, one after the other.
- **interpreted**: when you "run" the program/script, the interpreter read the text, and compute commands to send to the CPU, "on-the-fly".
- **reflective**: the current process can examine how it runs, and modify its own behavior, dynamically.

The interpreter comes with a useful **REPL (Read Eval Print Loop)**: a terminal waiting for input and evaluating your commands.

To compare with other language you may know, you can start there: https://en.wikipedia.org/wiki/Comparison_of_programming_languages#General_comparison

In practice, at startup CPython (the interpreter you will most likely use) will compile your code to an intermediate representation (intermediate binary language) called bytecode. These are stored in *.pyc* files.
They will be recompiled next time you run the program/script, so you usually don't need to think about these. it is just an "internal optimization" of CPython.

The reference/specification for the python programming language lives at https://www.python.org/dev/peps/
This is what is used by developers to implement the various interpreters and guarantee compatibility.

Python comes with the basic tools you need to program:

- **unittest**: a package to hep you write your tests (to make sure the program is doing what you expect)
- **doctest**: a package to make sure documentation is uptodate with your code.
- **documentation in code**: Most of the code is just documented text files, so you can always go and look at "how things are done under the hood".

The python online reference is actually generated from the actual interpreter code.

To distribute code that is larger than simple script files, the python community agreed on some "standard ways to do things".
The Guide is over there : https://packaging.python.org/tutorials/packaging-projects/


Virtual Environments
--------------------

Python is not a System Programming Language (like C or Rust), so it is not made to live at the heart of the machine.
It relies on a compiled C interpreter (CPython) to interract with your OS.

Therefore python programs can (and should) be isolated from each other.
A confusion can appear because most Linux/Unix distributions come with python preinstalled, to provide helpful functionality (scripts, GUIs, etc.).
However the package use for these functionalities do not have to be the same as the one you will use yourself.
This gives more freedom to the developer than if you were developing in C, where you have to use the exact same version of the library installed on your machine.

To isolate different python applications from each other, and let them choose the versions of the libraries they want to use, we use **virtual environments**.
Tutorial is there : https://docs.python.org/3/tutorial/venv.html

One need to always keep in mind the 3 Virtual environments potentially in use when running python:

- the system one (installed by your distribution for all users)
- the user one (where you can install your own python packages via pip)
- the current one for the application yuo are developing.

A detailed tutorial on how this all works : https://packaging.python.org/tutorials/installing-packages/

Currently perhaps the most practical way to manager virtual environments is https://pipenv.readthedocs.io/en/latest/

REPL
----

CPython comes with a basic REPL, that is very useful for "trying things out", "understand how things work", and even debugging your own Python setup.
For example if you are not sure which python version you are running, from which directory, and which modules are imported, you can always do::

    $ python
    Python 3.7.5 (default, Nov 20 2019, 09:21:52)
    [GCC 9.2.1 20191008] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import os
    >>> os.__file__
    '/<path_to_the_virtual_environment>/lib/python3.7/os.py'
    >>>

There are also other python REPLs implementation, with richer interface and more features.
One widely used one is IPython: https://ipython.readthedocs.io/en/stable/overview.html that provides all sorts of extra features.


Jupyter notebooks
-----------------

Python by itself does not provide any simple way to do visualization. Historically the Tcl/Tk library was used, interfaced in python via tkinter: https://docs.python.org/3/library/tkinter.html
There is also PyQT, and others for more complex GUIs. However in the web era, most people expected a more user-friendly and standard visualization tool.

IPython eventually evolved and the Jupyter project was created, to provide a richer interactive interface with the user.
https://en.wikipedia.org/wiki/Project_Jupyter

But one needs to keep in mind that what is actually happening behind the scene is not simple: https://jupyter.readthedocs.io/en/latest/architecture/how_jupyter_ipython_work.html

Given this architecture, various visualization packages have been developed to support the various usecases (notebook, qt, webserver, etc.).
This area is still under heavy development by various communities, and there isn't an obvious best choice just yet.

On noticeable recent development has been the recent call for compatibility between the notebook fileformat (initially json), and usual python code, for file versioning, debugging, importing, etc.
A good project for this is https://jupytext.readthedocs.io/en/latest/ and you can customize your workflow between notebooks and usual code to fit your needs.
