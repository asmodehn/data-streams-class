Hosting Data and Provide Visualization
======================================

Visualization can be a lot of things:

- Statistics
- Analysis
- Forecasts
- etc.

Regarding the server, there are many protocols you can rely on to do this:

- FTP
- HTTP
- WebDav
- SQL
- NNTP
- etc.

You will need to setup the appropriate server on your machine to host the data and eventually provide access.

Examples
--------

Here are some nice examples of data retrieval, hosting, while providing added value (vizualisation and more)

- https://data.laregion.fr/explore/
- https://openaq.org


WebSite
-------

Setting up a web server and providing content on it is quite simple nowadays.
Most online hosting service provide this service, and you can set it up on your machine with very minimal setup.

In this class we will focus on setting up a server for our data using flask.
This assumes our data is changing fast enough to require the use of a server.

If our data is static, we can just generate some html code and host a bunch of static webpages.
Another option is to use a project like https://datasette.readthedocs.io/en/stable/
Eve is also a popular way to provide structured data via REST: https://docs.python-eve.org/en/stable/

So in our case we will start with a cvs data sample, to setup the server and develop the API.
Finally to provide additional value we will develop a dashboard visualization.

Boilerplate: The Webserver
--------------------------

Connection to a webserver to retrieve some content is a common practice nowadays.

In python, two  of the widely used libraries are:
  - django, a full feature web framework
  - flask, a web framework aiming at simplicity

Here we will use **flask** for simplicity

Implement a simple website (for data presentation) and prepare an API endpoint.
Tuto: https://flask.palletsprojects.com/en/1.1.x/quickstart/#a-minimal-application


Run the server like this::

    $ python -m dataserver

And query it like so::

    $ curl http://127.0.0.1:5000/
    Hello World!
    $ curl http://127.0.0.1:5000/api
    Hello API!

NB : A flask server started with debug=True will auto-reload when a change in python code is detected.


Backend: REST API Basics
------------------------

Our final data will have a specific structure.
We need to retrieve the data and store it in some format.
Example with CSV : https://data.laregion.fr/explore/dataset/temperature-quotidienne-regionale/export/

Make your data available via the REST API.
Hint: pay attention to the structure of you data /vs/ the structure of a URL.

REST API:
  - Implement a basic REST API in Flask. (Tuto: https://blog.miguelgrinberg.com/post/designing-a-restful-api-with-python-and-flask )
  - Serve the static data from the CSV file, and design a simple REST API to access the data.

With the server running, access to an endpoint should allow you to select by region::

    $ curl http://127.0.0.1:8000/api/Occitanie
    {
      "2016-01-01": {
        "code_insee": 76,
        "region": "Occitanie",
        "tavg": 8.94,
        "tmax": 13.34,
        "tmin": 4.54
      },
      "2016-01-02": {
        "code_insee": 76,
        "region": "Occitanie",
    [...]
    }

Connection to a webserver to retrieve some content over a REST API (usually JSON) is a common practice nowadays.

From a client viewpoint, to retrieve data over a REST API they need to to a request and wait for a response.
This is simple to setup and develop, because it relies on the very stable and widespread HTTProtocol.

The only downside is that to retrieve "recent" data, one has to make repeated requests.

Then to build webpages, we can directly generate them on the server, and send the page to the client.

Webpages:
  - Add a table visualization (using the *to_html()* method of pandas dataframe).
  - you may want to add some CSS to make it less ugly...

Notice how this is different from having a *static* page somewhere, and using javascript in that page to request data via the REST API.
These are two different strategies and you will need to choose the most appropriate, depending on your usecase (usability, kind of data, how we access the data, etc.)

At this stage it is good to practice networking basics, and HTTP protocol. For example:
   - how the server can be accessed ? browser, text-browser, curl, etc.
   - what type of request can the client send ? header in request could change response format (xml, json, etc.)

FrontEnd: From notebook to Web page
-----------------------------------

For interactive visualization, we will use https://bokeh.org/
Currently it seems to be the most mature and documented data visualization package usable with notebooks or webservers.
There are many others out there and I encourage to try them out as well.

Bokeh:
 - Create a visualization for the data, with some interactive feature. This can be done in a jupyter notebook for quick visual feedback.
 - Inspect the generated code

It is important to note that flask dynamically generates webpages from templates, and we could generate webpages displaying visualization

We want to run a bokeh application in a dedicated server, to be able to practice manipulating web server apps.
These allow great flexibility in how we chose to handle our data, especially when it is realtime and we need to manage traffic.
However for simplicity we will here only consider using our csv data.

Bokeh documentation is there: https://docs.bokeh.org/en/latest/docs/user_guide.html

Integrate Flask and Bokeh together:
  - Add a webpage in flask to embed a sample bokeh visualization, and run the bokeh embed server on a background thread.
  - Replace the sample visualization with your visualization code from the notebook your made previously.

Documentation there : https://docs.bokeh.org/en/latest/docs/user_guide/server.html#embedding-bokeh-server-as-a-library


More Visualization / Dashboard packages
---------------------------------------

This is still an active development area where lots of different solutions are being experimented on by various communities.
I would advise to keep looking at this page as a reference : https://pyviz.org/dashboarding/index.html

Currently these tools seem quite promising, although they are still under heavy development, and may or may not match your usecases:

- https://github.com/voila-dashboards/voila
- https://www.streamlit.io/
- https://altair-viz.github.io/


WebSockets
----------

A REST API on your server is sometimes not enough, especially when you want to provide access to "realtime" data, or data in "push" mode.
HTTP2 is coming, with a system that enable pushing data to the client from the server.
However it is not widespread yet, so websockets are still widely in use for this purpose.

We will not see websockets in this class, as it is more "low-level" than REST, and there aren't really any standard protocols for this (except maybe https://wamp-proto.org/ ).
It is more complicated to develop your own network protocol, so we won't be dealing with realtime or pushed data in this class.
If you are looking towards this you might want to have a look at the Web Application Messaging Protocol https://wamp-proto.org/ .




