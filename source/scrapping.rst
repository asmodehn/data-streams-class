Scrapping Data from a Source
============================

There are many different kinds of data we can gather and retrieve from around the internet. Here we will practice with 4 different kinds of data.


Offline Data
------------

Source : http://www.gridwatch.templar.co.uk/france/

Write a program in python to:

- Retrieve the CSV file
- Read the file
- Plot a (clean) graph


packages :
 - **requests**: to download the file
 - **pandas**: to hold the data
 - **matplotlib**: to display a graph


Online Structured Data Static
-----------------------------

`SQLite Data Starter Packs <http://2016.padjo.org/tutorials/sqlite-data-starterpacks/#toc-california-school-sat-performance-and-poverty-data>`_

TODO : SQLite


Online Structured Data Dynamic
------------------------------

Source: https://www.metaweather.com/api/

packages:
 - **requests**: to retrieve json data via REST api
 - **pandas**: to hold the data
 - **matplotlib**: to display a graph


Online Unstructured Data
------------------------

Go to https://www.historique-meteo.net/ an pick a region of the world.
Notice how the HTML page is structured.

Retrieve the data from python, directly from internet.

packages:
 - **requests**: to get the webpage
 - **beautifulsoup4**: to parse the webpage
 - **pandas**: to hold the data
 - **matplotlib**: to display a graph



Other Potential Sources:

  - `Occitanie Data <https://data.laregion.fr/explore/?disjunctive.publisher&sort=modified&q=meteo>`_
  - `Meteo France <http://donneespubliques.meteofrance.fr>`_


