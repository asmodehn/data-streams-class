************
Data Formats
************

There are numerous data formats. Because anyone, if he doesnt care about intercompatibility, can just come up with his own.

For the uses of Internet, it quickly became necessary to standardise data formats, as files could be sent as part of an email, and the client needs to know how to use/display it.

This standard is called MIME, but since then it has largely grown in usage, it is being used by browsers to identify content to display. Therefore it is perhaps most largely known and used as "Media Type" in browser.

Traditionally systems have been differentiating binary files and "text" files. Text files are also binary files, since they also are represented on the disk as binary code.

But they have the additional property to represent a text, and are therefore usable by a large variety of software that a human can understand. It can be simple editors, or xml interpretors, configuration files, etc.

Binary files that are not text however, need much more effort to be understood by humans, and are instead aimed at being understood by a machine.

Here we will quickly overview just some of the common ones.


Binary
======

GIF
---

image/gif
https://en.wikipedia.org/wiki/GIF

bitmap image format well suited for simple images and animations

PNG
---

Media type: image/png

https://en.wikipedia.org/wiki/Portable_Network_Graphics

A raster-graphics file format that supports lossless compression.
It was developed as a better, non-patented GIF format



JPEG
----
image/jpeg

https://en.wikipedia.org/wiki/JPEG

A lossy compression file format for images.


MPEG
----

audio/mpeg

https://en.wikipedia.org/wiki/Moving_Picture_Experts_Group#Standards

Various video format.

OGG
---

audio/ogg

https://en.wikipedia.org/wiki/Ogg

Ogg is a free, open container format designed to provide for efficient streaming and manipulation of high-quality digital multimedia. 

PDF
---

application/pdf

https://en.wikipedia.org/wiki/PDF

The Portable Document Format (PDF) is a file format developed by Adobe in the 1990s to present documents, including text formatting and images, in a manner independent of application software, hardware, and operating systems. PDF was standardized as ISO 32000 in 2008, and no longer requires any royalties for its implementation.

ZIP
---

application/zip

https://en.wikipedia.org/wiki/Zip_(file_format)

ZIP is an archive file format that supports lossless data compression.



ODT
---

application/vnd.oasis.opendocument.text

https://en.wikipedia.org/wiki/OpenDocument

The Open Document Format for Office Applications (ODF), also known as OpenDocument, is a ZIP-compressed XML-based file format for spreadsheets, charts, presentations and word processing documents. It was developed with the aim of providing an open, XML-based file format specification for office applications.




Encodings
=========


Although Media types actually describe the *usage* of a file and not its nature, it is a useful reference because it is standardised. However it is also not very practical to understand the *nature* of a file format. For that you have to refer to each file format specifically.

For example, the media type **text/plain** just means "text-only data". It can have any kind of usage, the point of hte media type is that "what to do with it" is unspecified for the browser. 

But there are actually **many ways to encode text** in a binary file.



ASCII
-----

https://en.wikipedia.org/wiki/ASCII

ASCII (/ˈæskiː/ (About this soundlisten) ASS-kee),[2]:6 abbreviated from American Standard Code for Information Interchange, is a character encoding standard for electronic communication. ASCII codes represent text in computers, telecommunications equipment, and other devices. Most modern character-encoding schemes are based on ASCII, although they support many additional characters.

IANA standardized this as US-ASCII given its obvious aim at roman alphabet based languages and especially english.

There are many other character sets for different languages

https://www.iana.org/assignments/character-sets/character-sets.xhtml


UTF-8
-----

https://en.wikipedia.org/wiki/UTF-8

UTF-8 (8-bit Unicode Transformation Format) is a variable width character encoding capable of encoding all 1,112,064 valid code points in Unicode using one to four 8-bit bytes. The encoding is defined by the Unicode Standard, and was originally designed by Ken Thompson and Rob Pike. The name is derived from Unicode (or Universal Coded Character Set) Transformation Format – 8-bit.

UTF-8 is arguably the most influential technology to be developed recently. It is backward compatible with ASCII, can represent many different kinds of scripts (asian texts and more) and has been largely adopted. As of January 2020, 94.8% of all webpages are using utf-8.

It has been so influential, and designed well enough, that developers are pushing for its adoption literally everywhere, mostly to simplify the various existing character sets that a developer need to deal when dealing with texts for an international audience.

https://utf8everywhere.org/


Text
====

These formats are based on text representation. Therefore they already rely on ASCII, UTF-8, or any other binary representation of text.


javascript
----------

application/javascript

https://en.wikipedia.org/wiki/JavaScript

A media type for text files that contain javascript code


json
----

application/json

https://en.wikipedia.org/wiki/JSON

A media type for text files that contain javascript object notation


XML
---

text/xml
application/xml

https://en.wikipedia.org/wiki/XML

Extensible Markup Language (XML) is a markup language that defines a set of rules for encoding documents in a format that is both human-readable and machine-readable.

SQL
---

application/sql

https://en.wikipedia.org/wiki/SQL

SQL is a domain-specific language used in programming and designed for managing data held in a relational database management system (RDBMS), or for stream processing in a relational data stream management system (RDSMS). It is particularly useful in handling structured data, i.e. data incorporating relations among entities and variables. 

CSS
---

text/css

https://en.wikipedia.org/wiki/Cascading_Style_Sheets

Cascading Style Sheets (CSS) is a style sheet language used for describing the presentation of a document written in a markup language like HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.

HTML
----

text/html

https://en.wikipedia.org/wiki/HTML

Hypertext Markup Language (HTML) is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets (CSS) and scripting languages such as JavaScript.


CSV
---

text/csv

https://en.wikipedia.org/wiki/Comma-separated_values

A comma-separated values (CSV) file is a delimited text file that uses a comma to separate values. Each line of the file is a data record. Each record consists of one or more fields, separated by commas. A CSV file typically stores tabular data (numbers and text) in plain text, in which case each line will have the same number of fields.


How to introspect a file format ?
=================================

Shell
-----

The command "file" already gives some details.
If you want more information, you need to find programs that are able to extract that data.

For example for images, imagemagick, or xnview have a large panel of supported features for various image formats.



Python
------

Like with most programming language, and if the file format is open, you can read the spec of the file format yourself to implement a program to extract data from it.

The implementation is much easier if you have access to libraries to manipulate the format, because they are freely available , modifiable and shareable by the community.

TODO : extract data from image file
https://github.com/Juless89/python-tutorials/blob/master/EXIF/tutorial.md
https://www.youtube.com/watch?v=hY4WuX_KqUQ

https://github.com/vstinner/hachoir

TODO : extract data from open office calc sheet.



Where to go from here ?
=======================


Wikipedia:
  - https://en.wikipedia.org/wiki/MIME
  - https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
  - https://en.wikipedia.org/wiki/List_of_file_formats



