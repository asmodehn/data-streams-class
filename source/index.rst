.. Project Management Introduction documentation master file, created by
   sphinx-quickstart on Fri Nov 29 15:26:48 2019.

Data Streams Basic Practice
===========================

Data must be acquired before being provided.
After a sensor / a measurement "device", there is just a long chain of intermediary steps.

OpenData is a social movement that launched relatively recently (Tim Berners Lee called for it in 2009 at a TEDx event - Ref : `Wikipedia <https://en.wikipedia.org/wiki/Open_data>`_ )

Nowadays Governements publish openly part of their data. Ref : `Open Knowledge Foundation <https://index.okfn.org/place/>`_


.. toctree::
    :maxdepth: 2
    :numbered:
    :titlesonly:

    formats.rst
    python.rst
    scrapping.rst

    network.rst
    programming.rst
    hosting.rst


Quick Search
============

* :ref:`search`
