# # Imports

import os
from bokeh.layouts import column, row
from bokeh.plotting import figure, show
from bokeh.models import ColumnDataSource, Slider, Div, RangeTool
from bokeh.models.widgets import CheckboxGroup, RadioButtonGroup, RadioGroup
from bokeh.io import output_notebook
from IPython.core.debugger import set_trace  # to debug

# # Retrieving Data

from tempdata import RegionTemp
#dir_path = os.path.dirname(os.path.realpath(__file__))
dir_path = os.path.dirname(os.getcwd())
tempdata = RegionTemp("temperature-quotidienne-regionale.csv")

# # Splitting per Code INSEE

# +
df = tempdata.dataframe
tpt_per_code = dict()

# better by code insee to sidestep potential spelling mistakes
for c in df.code_insee.unique():
    tpt_per_code[c] = df[df['code_insee'] == c].sort_index()
    # Dont forget sorting !


# Data example (Occitanie)
tpt_per_code[76].head()
# -

# # Filtering relevant data, per region

# +
    
tpt_per_region = dict()
for k, d in tpt_per_code.items():
    print(k)
    # building temperature per region dict
    r = d.region.unique()
    if len(r) != 1:
        raise RuntimeError(f"Too many regions for {k} : {r}")
    else:
        # Note : we could also build code <-> region maps if needed
        tpt_per_region[r[0]] = d[["tmin", "tavg", "tmax"]].sort_index()
        # Dont forget sorting !

# Data example
tpt_per_region["Occitanie"].head()
# -

output_notebook()

# # Plotting

# plot scales original aspect based on available width
plot = figure(plot_height=240, tools="xpan", toolbar_location=None,
              x_range=(tpt_per_region["Occitanie"].index[0], tpt_per_region["Occitanie"].index[100]),
              x_axis_type="datetime", x_axis_location="above",
              background_fill_color="#efefef", sizing_mode="scale_width")
plot.vline_stack(["tmin", "tavg", "tmax"], x="date", source=tpt_per_region["Occitanie"],
                 color=("blue", "green", "red"))
show(plot)

# # Adding RangeTool for interactive visualization

select = figure(title="Drag the middle and edges of the selection box to change the range above",
                plot_height=130, sizing_mode="scale_width", y_range=plot.y_range, 
                x_axis_type="datetime", y_axis_type=None,
                tools="", toolbar_location=None, background_fill_color="#efefef")


# sharing range is enough to link behavior
range_tool = RangeTool(x_range=plot.x_range)
range_tool.overlay.fill_color = "navy"
range_tool.overlay.fill_alpha = 0.2

select.line(x='date', y='tavg', source=tpt_per_region["Occitanie"])
# select.line(x='date', y='tavg', source=tpt_per_region["Occitanie"].resample("W").mean())
select.ygrid.grid_line_color = None
select.add_tools(range_tool)
select.toolbar.active_multi = range_tool

show(column(plot, select, sizing_mode="scale_width"))

# # Adding legends to select a Region

# +
# Ref : https://docs.bokeh.org/en/latest/docs/user_guide/interaction.html

from bokeh.palettes import turbo

multiplot = figure(plot_height=240, tools="xpan", toolbar_location=None,
                   # here we take Occitanie as example to get a x_range, but any would do
                   # since we have the similar X dataseries for all)
              x_range=(tpt_per_region["Occitanie"].index[0], tpt_per_region["Occitanie"].index[100]),
              x_axis_type="datetime", x_axis_location="above",
              background_fill_color="#efefef", sizing_mode="scale_width")

for (region, data), color in zip(tpt_per_region.items(), turbo(len(tpt_per_region))):
    
    multiplot.line(y="tavg", x="date", source=data, color=color, legend_label=region)

multiplot.legend.location = "top_left"
multiplot.legend.click_policy="hide"

show(column(multiplot, sizing_mode="scale_width"))

# +

multiselect = figure(title="Drag the middle and edges of the selection box to change the range above",
                plot_height=130, sizing_mode="scale_width", y_range=multiplot.y_range, 
                x_axis_type="datetime", y_axis_type=None,
                tools="", toolbar_location=None, background_fill_color="#efefef")

# sharing range is enough to link behavior
multirange_tool = RangeTool(x_range=multiplot.x_range)
multirange_tool.overlay.fill_color = "green"
multirange_tool.overlay.fill_alpha = 0.2

multiselect.line(x='date', y='tavg', source=tpt_per_region["Occitanie"])
multiselect.ygrid.grid_line_color = None
multiselect.add_tools(multirange_tool)
multiselect.toolbar.active_multi = multirange_tool

show(column(multiplot, multiselect, sizing_mode="scale_width"))
