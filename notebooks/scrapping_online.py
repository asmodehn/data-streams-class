# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: 'Python 3.7.5 64-bit (''data-streams-class'': pipenv)'
#     language: python
#     name: python37564bitdatastreamsclasspipenv35ef66d2c121475498f5b740a8a7977c
# ---

# # Retrieve the webpage
#

# +

import requests

page = requests.get(
    "https://www.historique-meteo.net/france/languedoc-roussillon/montpellier/2018/"
)

# -

# ## Check we got what we expect

# +

from bs4 import BeautifulSoup

soup = BeautifulSoup(page.content, "html.parser")
print(soup.head)
print(soup.title)
# -

# ## Extract only what we need

# +

months = [
    "janvier",
    "fevrier",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "aout",
    "septembre",
    "octobre",
    "novembre",
    "decembre",
]


def h3_table(tag):
    return (tag.name == "h3" and tag.has_attr("id") and tag["id"] in months) or (
        tag.name == "table" and tag.has_attr("class") and "table" in tag["class"]
    )


data = soup.find_all(h3_table)
data
# -

# ## Build our data in dict, one by one
#

# +

columns = set()
monthly = dict()

datapoint = dict()

key = None
key2 = None
for d in data:
    # First one should be "janvier"
    if d.name == "h3":

        if datapoint:  # if we have previous data, we store it and start fresh
            monthly[key] = datapoint.copy()
            datapoint = dict()

        key = d["id"]
    elif d.name == "table":
        all_td = d.find_all("td")
        for td in all_td:
            # find the columns name
            if td.has_attr("style") and "width:65%" in td["style"]:
                columns.add(td.contents[0])
                key2 = td.contents[0]
            # find the actual data for this columns
            elif (
                td.has_attr("class")
                and "bg-primary" in td["class"]
                and td.contents[0].name != "span"
            ):
                datapoint[key2] = td.contents[0].text
monthly
# -

# # Getting a proper dataframe

# +

import pandas as pd

# store in pandas dataframe
df = pd.DataFrame.from_dict(data=monthly, orient="index", columns=columns)
print(df.head())
# -

# #  Getting numbers and plotting

df["Température moyenne"] = pd.to_numeric(
    df["Température moyenne"].apply(lambda s: s[:-1])
)
df

# # Plotting

df.plot(figsize=(15, 5), use_index=True, y="Température moyenne")
