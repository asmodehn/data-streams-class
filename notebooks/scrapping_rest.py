# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: 'Python 3.7.5 64-bit (''data-streams-class'': pipenv)'
#     language: python
#     name: python37564bitdatastreamsclasspipenv35ef66d2c121475498f5b740a8a7977c
# ---

# # API
# After having a look at the api documentation we can build helper procedures

# +
import requests


def woeid_from_cityname(cityname: str):
    response = requests.get(
        f"https://www.metaweather.com/api/location/search/?query={cityname}"
    )
    resdata = response.json()
    return resdata[0].get("woeid")


def get_weather(woeid: int):
    response = requests.get(f"https://www.metaweather.com/api/location/{woeid}/")
    resdata = response.json()

    return resdata.get("consolidated_weather")


# -

# ## Pick a city and get a Where On Earth ID

# +

cityname = "toulouse"

woeid = woeid_from_cityname(cityname)
woeid
# -

# ## Get consolidate weather JSON data

# +

weather = get_weather(woeid)
weather
# -

# # Build a dataframe

# +
import pandas as pd

weather_df = pd.DataFrame(data=weather)

# Print weather forecast
for p in weather_df.itertuples():
    print(f" {p.applicable_date}: {p.weather_state_name}")
