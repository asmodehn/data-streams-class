# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: 'Python 3.7.5 64-bit (''data-streams-class'': pipenv)'
#     language: python
#     name: python37564bitdatastreamsclasspipenv35ef66d2c121475498f5b740a8a7977c
# ---

# # Download
#
# This downloads a file to disk

import requests

csv_url = "http://www.gridwatch.templar.co.uk/downloads/french-grid.csv"
csv_file = requests.get(csv_url, allow_redirects=True)
open("french_grid.csv", "wb").write(csv_file.content)

# +
import pandas as pd

csvfname = "french_grid.csv"
df = pd.read_csv(
    csvfname, index_col="id", header=0, skipinitialspace=True
)  # , parse_dates=["time"])
df.head()
# -

# ## Cleanup
# Here we want to clean up the data to the proper format we 'd like

print(df.columns.values)  # need to make sure our column names are clean
df["time"] = pd.to_datetime(df["time"])
df.head()

# +

print(f"{df.shape}")
print(f"{df.dtypes}")
print(f"{df.isna().sum()}")

# -

# # Plot 1
#  lets attempt a plot...

# +

df.plot(figsize=(15, 5), kind="line", x="time", y="demand")
# -

# # Wait... What is that ?
# Looks like the data in file we downloaded is dirty. We miss some values which shifted all other data...
# Lets try to fix this

# +

from tqdm.autonotebook import tqdm

tqdm.pandas(desc="Cleaning everything !")

prev_row = None


def cleanup(row):
    global prev_row
    #  we assume demand doesnt deviate too much
    if prev_row is not None and row["demand"] < prev_row["demand"] * 0.3:
        # we assume it is an error in the data and we shift column values
        # we assume previous one is correct
        row = prev_row
    prev_row = row.copy()
    # print(prev_row["time"])
    return row


from tqdm.auto import tqdm

tqdm.pandas(desc="Cleaning everything !")


clean_df = df.progress_apply(cleanup, axis=1).drop_duplicates()
# -

# # Again
# Now that we have cleaner data, we can plot it

# +

clean_df.plot(figsize=(15, 5), kind="line", x="time", y="demand")
# -

# # Demand graph
# We want a clearer overview, less crowded with data.

demand = clean_df[["time", "demand"]].copy().reset_index(drop=True)
demand.demand = pd.to_numeric(demand.demand)  # make sure everything is a number
demand.set_index("time")
demand.head()

demand.plot(figsize=(15, 5), kind="line", x="time", y="demand")
demand.head()

# # Fixing index

demand.index = pd.to_datetime(demand.time, unit="s")
demand.head()

weekly_demand = demand[["demand"]].resample("W").mean()
weekly_demand.head()
weekly_demand.plot(figsize=(15, 5), kind="line", use_index=True, y="demand")
